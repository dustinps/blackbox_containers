#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE TABLE sogo_view (
    c_uid varchar(255),
    c_name varchar(255),
    c_password varchar(255),
    c_cn varchar(255),
    mail varchar(255)
  );

  INSERT INTO sogo_view VALUES ('sogo', 'sogo', 'dfbb885c6e4743f30025399a97c65ab0', 'sogo', 'sogo@swigg.net');
  INSERT INTO sogo_view VALUES ('anthony', 'anthony', '65fbef05e01fac390cb3fa073fb3e8cf', 'anthony', 'anthony@swigg.net');
EOSQL
